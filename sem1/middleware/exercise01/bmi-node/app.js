var express = require("express");
var app = express();
var path = require("path");


app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/calculate', function (req, res) {
  var name = req.query.name;
  var date = req.query.date.split('-').reverse().join('-');
  var height = Number(req.query.height);
  var weight = Number(req.query.weight);

  var BMI = weight / Math.pow(height, 2);
  BMI = Math.round(BMI * 100) / 100; // Rounded

  res.send(`Name: ${name}\nDate: ${date}\nBMI: ${BMI}`);
});


app.listen(3000);

console.log("Running at Port 3000");

