var express = require("express");
var app = express();
app.set('views', '/Users/nayeem/sites/hochschule-fulda/sem1/middleware/exercise02/bmi-node/views');
app.set('view engine', 'ejs');
var path = require("path");

var mysql = require('mysql')

var connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '12345678',
  database: 'bmi'
})

connection.connect(function (err) {
  if (err) throw err
  console.log('Mysql connected')
})

app.get('/', function (req, res) {
  // res.sendFile(path.join(__dirname + '/index.html'));
  connection.query('UPDATE data SET value = value + 1 WHERE id = 1', function (err, result) {
    connection.query('SELECT value FROM data WHERE type = "page-counter"', function (err, result) {
      res.render('index', { page_counter: result[0].value });
    });
  });
});

app.get('/calculate', function (req, res) {
  var name = req.query.name;
  var date = req.query.date.split('-').reverse().join('-');
  var height = Number(req.query.height);
  var weight = Number(req.query.weight);

  var BMI = weight / Math.pow(height, 2);
  BMI = Math.round(BMI * 100) / 100; // Rounded

  connection.query('INSERT INTO bmi (name, height, weight, date, bmi) VALUES (?, ?, ?, ?, ?)', [name, height, weight, date, BMI], function (err, result) {
    if (err) throw err
  })

  res.send(`Name: ${name}\nDate: ${date}\nBMI: ${BMI}`);
});


app.listen(3000);

console.log("Running at Port 3000");

// connection.query('SELECT * FROM people', function (err, results) {
//   if (err) throw err
//   console.log(results[0].id)
//   console.log(results[0].name)
//   console.log(results[0].age)
//   console.log(results[0].address)
// })